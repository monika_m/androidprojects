package com.example.photos


import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel
import android.graphics.drawable.Drawable
import android.widget.Toast
import com.google.gson.Gson
import kotlin.collections.ArrayList


class CardAdapter(private var list: MutableList<PhotoData>, private val sharedPreferences: SharedPreferences) :
    RecyclerView.Adapter<CardAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardAdapter.ViewHolder {
        val this_view: View = LayoutInflater.from(parent.context).inflate(R.layout.photo_card, parent, false)

        return CardAdapter.ViewHolder(this_view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = list[position].title
        holder.date.text = SimpleDateFormat("dd.MM.yyyy", Locale.US).format(list[position].date)



        val url = list[position].url
        if (!url.isBlank()) {

            Picasso.get().isLoggingEnabled = true
            Picasso.get().load(url).placeholder(R.drawable.ic_warning_black_24dp).into(object : com.squareup.picasso.Target {

                override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                    processImageTagging(bitmap, holder, position)
                    holder.photo.setImageBitmap(bitmap)
                }

                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                    holder.photo.setImageResource(R.drawable.ic_warning_black_24dp)
                }

                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                    holder.photo.setImageResource(R.drawable.ic_warning_black_24dp)
                }
            })
        }

        holder.photo.setOnClickListener{
            val intent = Intent(holder.photo.context, DetailsActivity::class.java)
            var tag:String=""
            if (list[position].tags!= null) {
                tag = list[position].tags.joinToString(", ")
            }
            var similars = findSimilar(position)
            Toast.makeText(holder.photo.context, list[position].tags.toString(), Toast.LENGTH_LONG).show()
            intent.putExtra("date", holder.date.text as String?)
            intent.putExtra("title", list[position].title)
            intent.putExtra("tags", tag)
            intent.putExtra("url", list[position].url)
            intent.putExtra("position", position)
            intent.putExtra("similars", similars)
            holder.photo.context.startActivity(intent)
        }
    }

    override fun getItemCount() = list.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val photo: ImageView = itemView.findViewById(R.id.imageView)
        val title: TextView = itemView.findViewById(R.id.TitleTV)
        val date: TextView = itemView.findViewById(R.id.DateTV)
        val tags: TextView = itemView.findViewById(R.id.tagsTV)

    }

    fun removeAt(position: Int) {
        // list.reverse()
        list.removeAt(position)

        notifyItemRemoved(position)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.putString(AddActivity.PHOTOS, Gson().toJson(list))
        editor.apply()
    }

    private fun processImageTagging(image: Bitmap?, holder: ViewHolder, position: Int) {

        image?.apply {

            val imagef = FirebaseVisionImage.fromBitmap(this)
            val labeler = FirebaseVision.getInstance().getOnDeviceImageLabeler()
            labeler.processImage(imagef)
                .addOnSuccessListener {
                    processTags(it, holder, position)
                }
                .addOnFailureListener {
                    Log.e("TAGS", it.toString())
                }
        }
    }

    private fun processTags(tags: List<FirebaseVisionImageLabel>, holder: ViewHolder, position: Int) {
        Log.d("TAGS", tags.map { it.text }.joinToString(" "))
        var tags_map = tags.map { it.text }

        val editor = sharedPreferences.edit()
        editor.clear()
        editor.putString(AddActivity.PHOTOS, Gson().toJson(list))
        list[position].tags = tags_map
        editor.putString(AddActivity.PHOTOS,Gson().toJson(list))
        editor.apply()

        if (tags_map.size > 2)
            tags_map = tags_map.subList(0, 3)
        holder.tags.text = tags_map.joinToString(", ")
    }

    private fun findSimilar(position: Int): Array<String> {

        val picture=list[position]
        val urls :Array<String> = emptyArray()

        try {
            val entriesSortedByMatchingTagsAmount = list.filter { item ->
                item.tags.filter { tag -> picture.tags.contains(tag) }.isNotEmpty() && item != picture
            }.sortedByDescending { item ->
                item.tags.filter { tag -> picture.tags.contains(tag) }.size
            }
            for (i in entriesSortedByMatchingTagsAmount.take(6)) {
                urls.plus(i.url)
            }
        } catch (e:Exception){

        }

        return urls
    }
}