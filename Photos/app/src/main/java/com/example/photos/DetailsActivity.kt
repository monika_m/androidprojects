package com.example.photos

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import com.example.photos.AddActivity.Companion.SHARED_PREFS
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.fragment_photo.*
import java.text.FieldPosition

class DetailsActivity : AppCompatActivity(), Photo.OnFragmentInteractionListener {
    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private var photo = com.example.photos.Photo()
    private var details= Details_photo()
    private var similar = SimilarPhotos()
    private var url=""
    private var date = ""
    private var title = ""
    private var tags = ""
    private var position = 0
    private var similars = emptyArray<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        var bundle: Bundle? = intent.extras
        url = bundle!!.getString("url")!!
        date = bundle!!.getString("date")!!
        title = bundle!!.getString("title")!!
        tags = bundle!!.getString("tags")!!
        position = bundle!!.getInt("position")!!
        similars = bundle!!.getStringArray("similars")!!

        photo = Photo.newInstance(url)
        details=Details_photo.newInstance(date,  title, tags)



       similar = SimilarPhotos.newInstance(similars)
        val fm = supportFragmentManager
        fm.beginTransaction()

            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            .replace(R.id.contener, photo)
            .commit()

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_fragments, menu) //build a menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {//"configure" menu options
        R.id.buttonFR -> {
            if (photo.isVisible) {
                val fm = supportFragmentManager
                fm.beginTransaction()

                    .replace(R.id.contener, details)
                    .replace(R.id.contener2, similar)
                    .commit()
            } else{
                val fm = supportFragmentManager
                fm.beginTransaction()
                .replace(R.id.contener, photo)
                .hide(similar)
                .commit()

            }

            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }



}
