package com.example.photos

import android.content.Context
import android.content.Intent.getIntent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.reflect.TypeToken
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_photo.*
import android.content.Intent.getIntent
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_photo.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "url"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [Photo.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [Photo.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class Photo : Fragment() {
    // TODO: Rename and change types of parameters
    private var url: String = ""
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      // url= arguments?.getString(ARG_PARAM1)!!
      //  loadImage(view!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view = inflater.inflate(R.layout.fragment_photo, container, false)
//        url= arguments?.getString(ARG_PARAM1)!!
//        loadImage(view)

        return view
    }

    override fun onStart() {
        super.onStart()
//        url= arguments?.getString(ARG_PARAM1)!!
//        loadImage(view!!)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var url= arguments?.getString(ARG_PARAM1)
//        urlTV.text="url! "+urlmenu_player.xml
        if (url!=null)
        if (!url.isBlank()) {
          //  Toast.makeText(context, "OK", Toast.LENGTH_LONG).show()
            // Picasso.get().load(url).into(holder.photo)
            Picasso.get().isLoggingEnabled = true
            Picasso.get().load(url).placeholder(R.drawable.ic_warning_black_24dp)
                .into(object : com.squareup.picasso.Target {

                    override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                        view.photo.setImageBitmap(bitmap)

                    }

                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                        view.photo.setImageResource(R.drawable.ic_warning_black_24dp)
                    }

                    override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                        view.photo.setImageResource(R.drawable.ic_warning_black_24dp)
                    }
                })
        }

    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
//            url= arguments?.getString(ARG_PARAM1)!!
           // loadImage(view!!)
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SimilarPhotos.
         */
        @JvmStatic
        fun newInstance(url: String) =
            Photo().apply {
                arguments = Bundle().apply {
                   putString(ARG_PARAM1, url)
                }      }
    }


}
