package com.example.photos

import java.util.*

class PhotoData (val url: String, val title: String,  val date: Date, var tags: List<String>)