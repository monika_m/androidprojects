package com.example.photos

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.icu.util.Calendar
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.format.DateFormat
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NavUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_add.*
import java.text.SimpleDateFormat
import java.util.*

class AddActivity : AppCompatActivity() {

    companion object {
        val SHARED_PREFS = "sharedPrefs"
        val PHOTOS = "savedPhotos"
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)

        calendarButton.setOnClickListener {
            clickDataPicker(View(this))
        }

        button.setOnClickListener {
            val url = url_adressET.text.toString()
            val title = titleET.text.toString()
            val date = SimpleDateFormat("dd.MM.yyyy", Locale.US).parse(dateET.text.toString()) as Date
            //Toast.makeText(this, date.toString(), Toast.LENGTH_LONG).show()
            val tags= emptyList<String>()
            val newPhoto= PhotoData(url, title, date, tags)

            val sharedPref = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            class Token : TypeToken<MutableList<PhotoData>>()
            var photos =sharedPref.getString(PHOTOS, null)
            var list= mutableListOf<PhotoData>()
            if(photos!=null) {
                list= Gson().fromJson(photos, Token().type)
            }
            list.add(0,newPhoto)

            editor.clear()
            editor.putString(PHOTOS,Gson().toJson(list))
            editor.apply()


            NavUtils.navigateUpTo(this, Intent(applicationContext, MainActivity::class.java))
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun clickDataPicker(view: View) {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val cal = Calendar.getInstance()
        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val df = SimpleDateFormat("dd.MM.yyyy", Locale.US)
            dateET.setText(df.format(cal.time))}, year, month, day)
        dpd.show()

    }

}

