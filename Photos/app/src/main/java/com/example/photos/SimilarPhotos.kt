package com.example.photos

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_photo.view.*
import kotlinx.android.synthetic.main.fragment_similar_photos.*


private const val URLS= "urls"

class SimilarPhotos : Fragment() {
    // TODO: Rename and change types of parameters
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_similar_photos, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var images:List<ImageView> = emptyList()
        images.plus(imageView3)
        images.plus(imageView4)
        images.plus(imageView5)
        images.plus(imageView6)
        images.plus(imageView7)
        images.plus(imageView8)


        var urls= arguments?.getStringArray(URLS)

        if (urls!=null) {
            Toast.makeText(view.context, urls.size.toString(), Toast.LENGTH_LONG).show()
            for (i in urls.indices) {
                loadPhoto(urls[i], images[i])
            }
        }
    }


    private fun loadPhoto(url:String, photo:ImageView){
        if (!url.isBlank()) {
            Picasso.get().isLoggingEnabled = true
            Picasso.get().load(url).placeholder(R.drawable.ic_warning_black_24dp)
                .into(object : com.squareup.picasso.Target {

                    override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                        photo.setImageBitmap(bitmap)
                    }

                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                        photo.setImageResource(R.drawable.ic_warning_black_24dp)
                    }

                    override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                        photo.setImageResource(R.drawable.ic_warning_black_24dp)
                    }
                })
        }
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
//        if (context is OnFragmentInteractionListener) {
//            listener = context
//        } else {
//            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
//        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        @JvmStatic
        fun newInstance(urls: Array<String>) =
            SimilarPhotos().apply {
                arguments = Bundle().apply {
                    putStringArray(URLS, urls)
                }
            }
    }
}
