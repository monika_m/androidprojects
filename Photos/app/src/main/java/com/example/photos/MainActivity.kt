package com.example.photos

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.app.NavUtils
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity(), Photo.OnFragmentInteractionListener{
    override fun onFragmentInteraction(uri: Uri) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val sharedPref = getSharedPreferences(AddActivity.SHARED_PREFS, Context.MODE_PRIVATE)
        class Token : TypeToken<MutableList<PhotoData>>()
        var scores =sharedPref.getString(AddActivity.PHOTOS, null)
        var list= mutableListOf<PhotoData>()
        if(scores!=null) {
            list= Gson().fromJson(scores, Token().type)
        }


        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter= CardAdapter(list, sharedPref)

        val swipeHandler = object : SwipeToDeleteCallback(this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = recyclerView.adapter as CardAdapter
                adapter.removeAt(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerView)


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.menu_addURL -> {
                val intent = Intent(applicationContext, AddActivity::class.java)
                startActivity(intent)
                true
            }
            R.id.check_item -> {
                pickImage()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    //code snippet to check firebase ml working
    private val PICK_PHOTO_FOR_AVATAR = 121
    private var image = null as Bitmap?

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                PICK_PHOTO_FOR_AVATAR -> {
                    val selectedImage = data!!.data
                    image = MediaStore.Images.Media.getBitmap(this.contentResolver, selectedImage)
                    //contentIV.setImageBitmap(image)
                    processImageTagging()
                }
            }
    }
    private fun processTags(tags: List<FirebaseVisionImageLabel>) {
        Log.d("TAGS", tags.map { it.text}.joinToString(" "))
        Toast.makeText(this, tags.map { it.text}.joinToString(" "), Toast.LENGTH_LONG).show()
    }

    private fun processImageTagging(){
        image?.apply{
            val imagef = FirebaseVisionImage.fromBitmap(this)
            val labeler = FirebaseVision.getInstance().getOnDeviceImageLabeler()
            labeler.processImage(imagef)
                .addOnSuccessListener {
                    processTags(it)
                }
                .addOnFailureListener {
                    Log.e ("TAGS", it.toString())
                }
        }
    }
    private fun pickImage() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR)
    }
}
