package com.example.myapplication;

import android.os.Bundle;
import android.app.FragmentTransaction;
import android.support.wearable.activity.WearableActivity;



public class MainActivity extends WearableActivity implements Photo1.OnFragmentInteractionListener, Photo2.OnFragmentInteractionListener, Photo3.OnFragmentInteractionListener {

    Photo1 f1;
    Photo2 f2;
    Photo3 f3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        f1 = Photo1.newInstance();
        f2 = Photo2.newInstance();
        f3 = Photo3.newInstance();
        FragmentTransaction trans =this.getFragmentManager().beginTransaction();
        trans.add(R.id.contener, f2);
        trans.detach(f2).hide(f2);
        trans.add(R.id.contener, f3);
        trans.detach(f3).hide(f3);
        trans.add(R.id.contener, f1);
        trans.commit();

        // Enables Always-on
        setAmbientEnabled();
    }

    @Override
    public void onFragmentInteraction1() {
        this.getFragmentManager().beginTransaction().detach(f1).hide(f1).attach(f2).show(f2).commit();
    }

    @Override
    public void onFragmentInteraction2() {
        this.getFragmentManager().beginTransaction().detach(f2).hide(f2).attach(f3).show(f3).commit();
    }

    @Override
    public void onFragmentInteraction3() {
        this.getFragmentManager().beginTransaction().detach(f3).hide(f3).attach(f1).show(f1).commit();
    }
}
