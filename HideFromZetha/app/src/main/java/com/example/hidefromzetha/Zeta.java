package com.example.hidefromzetha;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;


public class Zeta extends AppCompatActivity {

    private SensorManager sensorManager;
    private Sensor lightSensor;
    private SensorEventListener lightEventListener;
    private CountDownTimer timer;

    private final int CATCHING_TIME = 2000;
    final static String SHARED_PREFS="SHARED_PREFS";
    final static String SCORE = "score";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zeta);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        if (lightSensor == null) {
            Toast.makeText(this, getString(R.string.zeta_no_lightsens), Toast.LENGTH_SHORT).show();
            finish();
        }

        startTimer();

        addLightListener();

    }//onCreate

    private void startTimer() {
        timer=new CountDownTimer(CATCHING_TIME, 1000){
            public void onTick(long millisUntilFinished){
            }
            public  void onFinish(){
                Intent i = new Intent(Zeta.this, EndActivity.class);
                Zeta.this.startActivity(i);
                finish();
            }
        }.start();
    }

    private void addLightListener() {
        lightEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                float value = sensorEvent.values[0];
                if (value < 9) {
                    SharedPreferences sharedPref = getSharedPreferences("SHARED_PREFS", Context.MODE_PRIVATE);
                    int score= sharedPref.getInt("score",0);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("score", ++score);
                    editor.apply();
                    Toast.makeText(getApplicationContext(), String.valueOf(score), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Zeta.this, PlayActivity.class);
                    Zeta.this.startActivity(intent);
                    timer.cancel();
                    finish();
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {
            }

        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(lightEventListener, lightSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(lightEventListener);
    }

    @Override
    public void onBackPressed(){
        Intent i = new Intent(Zeta.this, MainActivity.class);
        Zeta.this.startActivity(i);
        timer.cancel();
        SharedPreferences sharedPref = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(SCORE, 0);
        finish();
    }
}
