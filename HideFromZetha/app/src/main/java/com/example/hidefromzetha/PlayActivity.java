package com.example.hidefromzetha;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;

public class PlayActivity extends AppCompatActivity {

    private int player;
    final static String PLAYER="player";
    private CountDownTimer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPref = getSharedPreferences(Zeta.SHARED_PREFS, Context.MODE_PRIVATE);
        player= sharedPref.getInt(PLAYER,0);
        setView();

        int time_out = (int)(Math.random()*4000+2000);
        timer=new CountDownTimer(time_out, 1000){
            public void onTick(long millisUntilFinished){
            }
            public  void onFinish(){
                Intent i = new Intent(PlayActivity.this, Zeta.class);
                PlayActivity.this.startActivity(i);
                finish();
            }
        }.start();
    }

    private void setView() {
        if (player == 0)
            setContentView(R.layout.activity_play);
        else if (player==1)
            setContentView(R.layout.activity_play_shimmer);
        else
            setContentView(R.layout.activity_play_shine);
    }

    @Override
    public void onBackPressed(){
        Intent i = new Intent(PlayActivity.this, MainActivity.class);
        PlayActivity.this.startActivity(i);
        timer.cancel();
        SharedPreferences sharedPref = getSharedPreferences(Zeta.SHARED_PREFS, Context.MODE_PRIVATE);
        int score= sharedPref.getInt(Zeta.SCORE,0);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(Zeta.SCORE, 0);
        finish();
    }
}
