package com.example.hidefromzetha;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class EndActivity extends AppCompatActivity {

    private final String BEST="best";
    private final String  SCORE_TXT="Twój wynik: \n";
    private final String RECORD_TXT="Rekord: \n";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);

        SharedPreferences sharedPref = getSharedPreferences(Zeta.SHARED_PREFS, Context.MODE_PRIVATE);
        int score= sharedPref.getInt(Zeta.SCORE,0);
        int best = sharedPref.getInt(BEST,0);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(Zeta.SCORE, 0);
        if (best<score) {
            best=score;
            editor.putInt(BEST, score);
        }
        editor.apply();

        TextView score_text = findViewById(R.id.score);
        score_text.setText(SCORE_TXT + score);
        TextView best_score_text = findViewById(R.id.bestScore);
        best_score_text.setText(RECORD_TXT + best);
        ImageButton play = findViewById(R.id.buttonPlay2);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(EndActivity.this, PlayActivity.class);
                EndActivity.this.startActivity(myIntent);
                finish();
            }
        });
        ImageButton back = findViewById(R.id.back_button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(EndActivity.this, MainActivity.class);
                EndActivity.this.startActivity(myIntent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed(){
        Intent i = new Intent(EndActivity.this, MainActivity.class);
        EndActivity.this.startActivity(i);
        finish();
}
}
