package com.example.hidefromzetha;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity {

    private int player=0;
    private SensorManager sensorMgr;
    private SensorEventListener accEventListener;
    private Sensor sensor;

    private float mAccel; // acceleration apart from gravity
    private float mAccelCurrent; // current acceleration including gravity
    private float mAccelLast; // last acceleration including gravity


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView();

        sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor=sensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        if (sensor == null) {
            Toast.makeText(this, getString(R.string.zeta_no_accelerator), Toast.LENGTH_SHORT).show();
            finish();
        }

        addActionListener();
    }

    private void addActionListener() {
        accEventListener = new SensorEventListener() {

            @Override
            public void onSensorChanged(SensorEvent event) {
                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];
                mAccelLast = mAccelCurrent;
                mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
                float delta = mAccelCurrent - mAccelLast;
                mAccel = mAccel * 0.9f + delta; // perform low-cut filter

                if (mAccel > 20) {
                    player=(player+1) % 3;
                    setView();
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {
            }

        };

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.player_selected) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setView() {
        if (player == 0)
            setContentView(R.layout.activity_play);
        else if (player==1)
            setContentView(R.layout.activity_play_shimmer);
        else
            setContentView(R.layout.activity_play_shine);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorMgr.registerListener(accEventListener, sensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorMgr.unregisterListener(accEventListener);
    }

    @Override
    public void onBackPressed(){
        SharedPreferences sharedPref = getSharedPreferences(Zeta.SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(PlayActivity.PLAYER, player);
        editor.apply();
        Intent i = new Intent(SettingsActivity.this, MainActivity.class);
        SettingsActivity.this.startActivity(i);
        finish();
    }

}
